import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { AuthProvider } from "./modules/auth/components/Auth";
import { AuthInit } from "./modules/auth/components/AuthInit";
import { setUpAxios } from './modules/auth/components/AuthHelper';
import 'bootstrap-icons/font/bootstrap-icons.css';

const root = ReactDOM.createRoot(document.getElementById('root'));
setUpAxios();
root.render(
    <React.StrictMode>
        <AuthProvider>
            <AuthInit>
                <App />
            </AuthInit>
        </AuthProvider>
    </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
