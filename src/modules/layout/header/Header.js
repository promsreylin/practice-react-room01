import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Button from 'react-bootstrap/Button';
import { useAuth } from '../../auth/components/Auth';


const Header = () => {

    const { logout } = useAuth();

    return (
        <Navbar expand="lg">
            <Container fluid>
                <Navbar.Brand className="fw-bolder fs-1">product</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll">
                    <Nav
                        className="me-auto justify-content-center flex-grow-1"
                        navbarScroll
                    >
                        <Nav.Link to="/" className="fs-3 fw-bolder text-secondary me-5">Home</Nav.Link>
                        <Nav.Link href="#action2" className="fs-3 fw-bolder text-secondary me-5">Contact</Nav.Link>
                        <NavDropdown title="Blog" id="navbarScrollingDropdown" className="fs-3 fw-bolder text-secondary">
                            <NavDropdown.Item href="#action3">Action</NavDropdown.Item>
                            <NavDropdown.Item href="#action4">
                                Another action
                            </NavDropdown.Item>
                            <NavDropdown.Divider />
                            <NavDropdown.Item href="#action5">
                                Something else here
                            </NavDropdown.Item>
                        </NavDropdown>
                    </Nav>
                </Navbar.Collapse>
                <Button variant='danger' onClick={logout}>
                    Logout
                </Button>
            </Container>
        </Navbar>
    );
}

export default Header;
