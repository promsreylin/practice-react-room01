import { useEffect, useState } from "react";
import { getCategories, getListProduct, getListProductByCategory, searchProduct } from "../core/request";
import { useNavigate } from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Badge from 'react-bootstrap/Badge';

import PaginationBasic from "./Pagination";


const ListProducts = () => {

    const [products, setProducts] = useState([]);
    const [active, setActive] = useState("All");
    const [categories, setCategories] = useState([]);
    const [searchText, setSearchText] = useState("");
    const [total, setTotal] = useState(0);
    const navigate = useNavigate();

    const limit = 12;

    useEffect(() => {
        getListProduct(limit).then((response) => {
            setProducts(response.data.products);
            setTotal(response.data.total);
        });

        getCategories().then((response) => {
            setCategories(response.data);
        })
    }, []);


    useEffect(() => {
        active === "All" ?
            getListProduct(limit).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);
            })
            : getListProductByCategory(limit, active).then((response) => {
                setProducts(response.data.products);
                setTotal(response.data.total);

            });
    }, [active]);


    const onSearch = () => {
        searchProduct({ q: searchText }).then((response) => {
            setProducts(response.data.products);
        });
    }




    return <div className="main">
        <div className="w-100 d-flex justify-content-center mb-4">
            <Form className="d-flex justify-content-center px-5 w-50">
                <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2 mt-5"
                    aria-label="Search"
                    onChange={(e) => setSearchText(e.target.value)}
                />
                <Button onClick={onSearch} className="mt-5 bg-white text-black border-2 border-info btn">Search</Button>
            </Form>
        </div>

        <div className="px-4">
            <div className="d-flex overflow-x-scroll w-100 pb-3">
                <Badge onClick={() => setActive("All")} className="px-4 py-2 fs-6 mx-2" bg={active === "All" ? "primary" : "secondary"}>All</Badge>
                {categories.map((item, index) =>
                    <Badge onClick={() => setActive(item)} key={index} className="px-4 py-2 fs-6 mx-2 " bg={active === item ? "primary" : "secondary"}>{item}</Badge>)}
            </div>
        </div>


        <div className="d-flex flex-wrap justify-content-center">
            {products.length === 0 && <div>
                Not Found
            </div>}
            {products.map((product, index) => {
                return <div onClick={() => navigate(`product/${product.id}`)} className="mx-3 shadow-lg my-2 bg-info-subtle" style={{

                }} key={index}>
                    <img style={{
                        height: "250px",
                        width: "320px"
                    }} className="" src={product.thumbnail} alt="__" />
                    <p className="px-2 fs-4 fw-bolder">{product.title}</p>
                    <p className="px-2 fs-5 text-bg-info text-bg-dark mx-auto"><i className="bi bi-heart-fill m-3 text-danger"></i>{product.rating}%</p>
                    <p className="px-2 fs-5 text-bg-primary">${product.price}</p>
                </div>
            })}
        </div>
        <div className="d-flex justify-content-center my-3">
            <PaginationBasic onChangePage={(page) => {
                getListProduct(limit, (page - 1) * limit).then((response) => {
                    setProducts(response.data.products);
                    setTotal(response.data.total);
                });

            }} total={total / limit} />
        </div>
    </div>
}

export default ListProducts;