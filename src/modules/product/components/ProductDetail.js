import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import {getProductById} from "../core/request";

const ProductDetail = () => {
    const {id} = useParams();
    const [product, setProduct] = useState({});


    useEffect(() => {
        getProductById(id).then((response)=>{
            setProduct(response.data);
        })
    }, [id]);

    if (!product.id){
        return  <div className="text-center fs-6">
            Loading...
        </div>
    }

    return (
    
    <div className="container d-flex justify-content-center border border-2 shadow-lg mt-5 p-5 mb-5" style={{width:'700px'}}>

       <div className="mx-auto">
           <img className="w-100" src={product.thumbnail} alt="" style={{height:'300px'}}/>
           <h2 className="text-center">{product.title}</h2>
           <p className="fs-3">{product.description}</p>

       </div>

    </div>
    )
}
export default ProductDetail;